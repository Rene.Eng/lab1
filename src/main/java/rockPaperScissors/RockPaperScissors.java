package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    Random rand = new Random();
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        
        String playing = "y";
        enum Moves {ROCK, PAPER, SCISSORS;}

        System.out.println("Let's play round 1");

        while (playing.equals("y")){
            String humanAnswer = readInput("Your choice (Rock/Paper/Scissors)?");
            String humanInternal = humanAnswer.toUpperCase();

            if (rpsChoices.contains(humanAnswer)) {
                
                Moves computerAnswer = Moves.values()[rand.nextInt(3)]; // Implement randomness
                
                if (humanInternal.equals(computerAnswer.name())){
                    System.out.println("Human chose " + humanAnswer + ", computer chose " + computerAnswer + ". It's a tie!");
                }
                else switch (computerAnswer){
                        case ROCK:
                            if (humanAnswer.equals("paper")){
                                System.out.println("Human chose paper, computer chose rock. Human wins!");
                                humanScore += 1;
                            }
                            else {
                                System.out.println("Human chose scissors, computer chose rock. Computer wins!");
                                computerScore += 1;
                            }
                            break;
                        case PAPER:
                            if (humanAnswer.equals("scissors")){
                                System.out.println("Human chose scissors, computer chose paper. Human wins!");
                                humanScore += 1;
                            }
                            else {
                                System.out.println("Human chose rock, computer chose paper. Computer wins!");
                                computerScore += 1;
                            }
                            break;
                        case SCISSORS:
                            if (humanAnswer.equals("rock")){
                                System.out.println("Human chose rock, computer chose scissors. Human wins!");
                                humanScore += 1;
                            }
                            else {
                                System.out.println("Human chose paper, computer chose scissors. Computer wins!");
                                computerScore += 1;
                            }
                            break;
                        }
                    roundCounter += 1;
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                    playing = readInput("Do you wish to continue playing? (y/n)?");
                    if (playing.equals("y")){
                        System.out.println("Let's play round " + roundCounter);
                    }
                    else if (!(playing.equals("n"))){
                        playing = readInput("I do not understand " + playing + ". Could you try again?");
                        if (playing.equals("y")){
                            System.out.println("Let's play round " + roundCounter);
                    }
                    }
                }
                else {
                    System.out.println("I do not understand " + humanAnswer + ". Could you try again?");
            }
             
            }
        System.out.println("Bye bye :)");
        }
    

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
